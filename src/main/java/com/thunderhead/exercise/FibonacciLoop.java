package com.thunderhead.exercise;

/**
 * Description:
 *
 * @author njamkhande
 */
public class FibonacciLoop implements Sequence {
	public int evenSum(int length) {
		int sum = 0;
		if (length <= 1) {
			return sum;
		}
		int[] fibonacciSequence = generateSequence(length);
		for (int i = 0; i < fibonacciSequence.length && sum < 4000000; i++) {
			if (fibonacciSequence[i] % 2 == 0) { sum = sum + fibonacciSequence[i]; }
		}
		return sum;
	}

	private int[] generateSequence(int length) {
		int[] fibonacciSeq = new int[length + 1];
		fibonacciSeq[0] = 0;
		fibonacciSeq[1] = 1;

		for (int i = 2; i <= length; i++) {
			fibonacciSeq[i] = fibonacciSeq[i - 1] + fibonacciSeq[i - 2];
		}

		return fibonacciSeq;
	}
}
