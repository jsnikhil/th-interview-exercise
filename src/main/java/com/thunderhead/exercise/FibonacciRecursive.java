package com.thunderhead.exercise;

/**
 * Description: 0,1,1,2,3,5,8,13
 *
 * @author njamkhande
 */
public class FibonacciRecursive implements Sequence {
	public int evenSum(int length) {
		if (length <= 2) {
			return 0;
		}
		return recurseAdd(length - 2, 0, 1, 1);
	}

	private Integer recurseAdd(int index, int accumulator, int previous, int current) {
		if (index == 0 || current > 4000000) {
			return accumulator;
		}
		int next = previous + current;
		if ((next) % 2 == 0) {
			accumulator = accumulator + next;
		}

		return recurseAdd(index - 1, accumulator, current, next);
	}
}
