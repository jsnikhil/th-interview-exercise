package com.thunderhead.exercise;

/**
 * Description:
 *
 * @author njamkhande
 */
public interface Sequence {
	int evenSum(int length);
}
