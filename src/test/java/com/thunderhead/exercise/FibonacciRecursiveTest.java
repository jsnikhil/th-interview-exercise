package com.thunderhead.exercise;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Description:
 *
 * @author njamkhande
 */
public class FibonacciRecursiveTest {
	@Test
	public void testSumCalc() throws Exception {
		Sequence fibonacci = new FibonacciRecursive();
		assertEquals(0, fibonacci.evenSum(1));
		assertEquals(0, fibonacci.evenSum(2));
		assertEquals(2, fibonacci.evenSum(3));
		assertEquals(2, fibonacci.evenSum(4));
		assertEquals(10, fibonacci.evenSum(6));
		assertEquals(44, fibonacci.evenSum(10));
		assertEquals(44, fibonacci.evenSum(11));
		assertEquals(4613732, fibonacci.evenSum(111));
	}
}